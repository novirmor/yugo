variable "qemu_accelerator" {
  type    = string
  default = "kvm"
}

variable "qemu_format" {
  type    = string
  default = "qcow2"
}
