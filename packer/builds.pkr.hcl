build {
  name    = "yugo"

  source "qemu.default" {
    name = "alpine"
  }
  source "qemu.default" {
    name = "centos"
  } 
  source "qemu.default" {
    name = "debian"
  }
  source "virtualbox-iso.default" {
    name = "alpine"
  }
  source "virtualbox-iso.default" {
    name = "centos"
  } 
  source "virtualbox-iso.default" {
    name = "debian"
  }

  provisioner "ansible" {
    playbook_file = "./ansible/main.yml"
  }

  post-processor "vagrant" {
    keep_input_artifact = true
    output = "output/vagrant/yugo.${source.name}.${source.type}.box"
  }
}
