variable "ssh_password" {
  type    = string
  default = "root"
}

variable "ssh_username" {
  type    = string
  default = "root"
}

variable "ssh_port" {
  type    = string
  default = "22"
}

variable "ssh_wait_timeout" {
  type    = string
  default = "1800s"
}

