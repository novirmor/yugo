source "qemu" "default" {
  accelerator      = var.qemu_accelerator
  boot_command     = var.boot_command
  boot_wait        = var.boot_wait
  cpus             = var.cpus
  disk_size        = var.disk_size
  format           = var.qemu_format
  headless         = var.headless
  http_directory   = "http/qemu"
  iso_checksum     = var.iso_checksum
  iso_urls         = ["${var.mirror_first_url}/${var.iso_name}", "${var.mirror_second_url}/${var.iso_name}"]
  memory           = var.memory
  output_directory = "output/qemu/${var.os_name}"
  shutdown_command = var.shutdown_command
  ssh_password     = var.ssh_password
  ssh_port         = var.ssh_port
  ssh_username     = var.ssh_username
  ssh_wait_timeout = var.ssh_wait_timeout
  vm_name          = "yugo.${var.os_name}.${var.qemu_format}"
}
