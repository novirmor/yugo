
os_name = "centos"

iso_name = "CentOS-8.2.2004-x86_64-minimal.iso"

mirror_first_url = "https://mirrors.edge.kernel.org/centos/8.2.2004/isos/x86_64"

mirror_second_url = "http://ftp.agh.edu.pl/centos/8.2.2004/isos/x86_64"

iso_checksum = "47ab14778c823acae2ee6d365d76a9aed3f95bb8d0add23a06536b58bb5293c0"

vbox_guest_os_type = "RedHat_64"

boot_command = [
    "<up><wait><tab> text ks=http://{{ .HTTPIP }}:{{ .HTTPPort }}/centos.cfg<enter><wait>"
]