
os_name = "alpine"

iso_name = "alpine-standard-3.12.0-x86_64.iso"

mirror_first_url = "http://dl-cdn.alpinelinux.org/alpine/v3.12/releases/x86_64"

mirror_second_url = "http://dl-2.alpinelinux.org/alpine/v3.12/releases/x86_64"

iso_checksum = "02d58cdafc471200489a02c4162e9211fc9c38a200f064e5585e3b7945ec41257c79286a5fe282129159bcacd787f909de74471eac35193b3a4377162d09fa29"

boot_command = [
  "root<enter><wait5>",
  "ifconfig eth0 up && udhcpc -i eth0<enter><wait5>",
  "wget http://{{ .HTTPIP }}:{{ .HTTPPort }}/alpine.cfg<enter><wait5>",
  "setup-alpine -f alpine.cfg<enter><wait10>",
  "root<enter><wait5>",
  "root<enter><wait>",
  "<enter><wait5>",
  "y<enter>",
  "<wait60><wait60><wait60><wait60><wait60>",
  "reboot<enter><wait60>",
  "root<enter><wait5>",
  "root<enter>",
  "apk add python3<enter><wait10>",
  "/etc/init.d/sshd stop<enter><wait>",
  "echo 'PermitRootLogin yes' >> /etc/ssh/sshd_config<enter>",
  "/etc/init.d/sshd start<enter><wait5>",
  "exit<enter>"
]