
os_name = "debian"

iso_name = "debian-10.6.0-amd64-netinst.iso"

mirror_first_url = "https://cdimage.debian.org/debian-cd/current/amd64/iso-cd"

mirror_second_url = "https://ftp.icm.edu.pl/pub/Linux/debian-cd/10.6.0/amd64/iso-cd"

iso_checksum = "cb74dcb7f3816da4967c727839bdaa5efb2f912cab224279f4a31f0c9e35f79621b32afe390195d5e142d66cedc03d42f48874eba76eae23d1fac22d618cb669"

vbox_guest_os_type = "Debian_64"

boot_command = [
  "<esc><wait60>",
  "auto url=http://{{ .HTTPIP }}:{{ .HTTPPort }}/debian.cfg",
  "<wait60><enter><wait60>"
]