variable "os_name" {
  type = string
}

variable "boot_command" {
  type = list(string)
}

variable "iso_name" {
  type = string
}

variable "mirror_first_url" {
  type = string
}

variable "mirror_second_url" {
  type = string
}

variable "headless" {
  type    = string
  default = "true"
}

variable "iso_checksum" {
  type = string
}

variable "boot_wait" {
  type    = string
  default = "15s"
}

variable "cpus" {
  type    = string
  default = "1"
}

variable "disk_size" {
  type    = string
  default = "4240"
}

variable "memory" {
  type    = string
  default = "1024"
}

variable "shutdown_command" {
  type    = string
  default = "poweroff"
}