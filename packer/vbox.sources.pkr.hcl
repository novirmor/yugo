source "virtualbox-iso" "default" {
  boot_command     = var.boot_command
  boot_wait        = var.boot_wait
  cpus             = var.cpus
  disk_size        = var.disk_size
  format           = var.vbox_format
  guest_os_type    = var.vbox_guest_os_type
  headless         = var.headless
  http_directory   = "http/vbox"
  iso_checksum     = var.iso_checksum
  iso_urls         = ["${var.mirror_first_url}/${var.iso_name}", "${var.mirror_second_url}/${var.iso_name}"]
  memory           = var.memory
  output_directory = "output/vbox/${var.os_name}"
  shutdown_command = var.shutdown_command
  ssh_password     = var.ssh_password
  ssh_port         = var.ssh_port
  ssh_username     = var.ssh_username
  ssh_wait_timeout = var.ssh_wait_timeout
  vm_name          = "yugo.${var.os_name}"
}