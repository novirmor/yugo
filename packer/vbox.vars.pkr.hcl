variable "vbox_format" {
  type    = string
  default = "ova"
}

variable "vbox_guest_os_type" {
  type    = string
  default = "Linux_64"
}