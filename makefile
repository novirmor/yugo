all: alpine centos debian
clean:
	rm -rf *output *.log

vbox: alpine_vbox centos_vbox debian_vbox
qemu: alpine_qemu centos_qemu debian_qemu

alpine: alpine_vbox alpine_qemu
alpine_vbox:
	packer build -var-file="./packer/vars/alpine.pkrvars.hcl" -only="yugo.virtualbox-iso.alpine" ./packer
alpine_qemu:
	packer build -var-file="./packer/vars/alpine.pkrvars.hcl" -only="yugo.qemu.alpine" ./packer

centos: centos_vbox centos_qemu
centos_vbox:
	packer build -var-file="./packer/vars/centos.pkrvars.hcl" -only="yugo.virtualbox-iso.centos" ./packer
centos_qemu:
	packer build -var-file="./packer/vars/centos.pkrvars.hcl" -only="yugo.qemu.centos" ./packer

debian: debian_vbox debian_qemu
debian_vbox:
	packer build -var-file="./packer/vars/debian.pkrvars.hcl" -only="yugo.virtualbox-iso.debian" ./packer
debian_qemu:
	packer build -var-file="./packer/vars/debian.pkrvars.hcl" -only="yugo.qemu.debian" ./packer
